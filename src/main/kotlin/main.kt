import java.util.*

object Application {

    private lateinit var scanner: Scanner

    @JvmStatic
    fun main(args: Array<String>) {


        var numbersList = getListFromInput()

        printOptionsMessage()

        val selectedOption = getSelectedOption()

        executeAlgorithm(selectedOption,numbersList)

    }

    private fun executeAlgorithm(selectedOption:Int,numbersList: MutableList<Int>) {
        when (selectedOption) {
            1 -> {
                usingBubble(numbersList)
            }
            2 -> {
                usingQuicksort(numbersList)
            }
            3 -> {
                usingBinarySearch(numbersList)
            }
            else -> println("El valor ingresado es incorrecto")
        }
    }

    private fun getSelectedOption(): Int {

        return scanner.nextInt()
    }

    private fun printOptionsMessage() {
        println("")
        println("Por favor ingrese el algoritmo que quiera utilizar")
        println("")
        println("Ingrese 1 para utilizar el algoritmo de Burbuja")
        println("Ingrese 2 para utilizar el algoritmo Quicksort")
        println("Ingrese 3 para utilizar el algoritmo de búsqueda binaria")
    }

    private fun getListFromInput():MutableList<Int>{

        scanner = Scanner(System.`in`)
        println("Ingrese una lista de numeros separados por coma:")
        var stringInput = scanner.next()

        while(stringInput.startsWith(",") || stringInput.endsWith(",")){
            println("No ingresar comas ni al principio ni al fin de la lista")
            println("")
            println("Ingrese una lista de numeros separados por coma:")
            stringInput = scanner.next()
        }


        var textList = stringInput.split(",").toMutableList()
        var numbersList = mutableListOf<Int>()
        val listSize = textList.size

        for (i in (0 until listSize)) {
            numbersList.add(textList[i].toInt())
        }

        println("lista desordenada: $numbersList")
        return numbersList
    }

    private fun usingBubble(numbersList: MutableList<Int>){

        val listSize = numbersList.size
        var temp = 0


        for (i in (0 until listSize - 1)) {
            for (j in (0 until listSize - 1)) {
                if (numbersList[j] > numbersList[j + 1]) {
                    temp = numbersList[j]
                    numbersList[j] = numbersList[j + 1]
                    numbersList[j + 1] = temp
                }
            }
        }
        println("lista ordenada mediante algoritmo de burbuja: $numbersList")
    }

    private fun quicksort(numbersList:List<Int>):List<Int>{
        if (numbersList.count() < 2){
            return numbersList
        }
        val pivot = numbersList[numbersList.count()/2]
        val equal = numbersList.filter { it == pivot }
        val less = numbersList.filter { it < pivot }
        val greater = numbersList.filter { it > pivot }

        return quicksort(less) + equal + quicksort(greater)
    }

    private fun usingQuicksort(numbersList: List<Int>){
        val orderedList = quicksort(numbersList)
        println("lista ordenada mediante algoritmo quicksort: $orderedList")
    }

    private fun usingBinarySearch(numbersList: List<Int>){

        println("Ingrese el numero que necesita encontrar")
        var numberToSearch = scanner.nextInt()

        val size = numbersList.size
        var inf = 0
        var center = 0
        var sup = size -1
        var flag = false

        while(inf<=sup){
            center = (sup+inf)/2
            if(numberToSearch==numbersList[center]){
                flag=true
                break
            }else if(numberToSearch<numbersList[center]) {
                sup = center - 1
            }else{
                inf = center + 1
            }
        }
        if(flag){
            println("El numero $numberToSearch está en la posición ${center+1}")
        }else{
            println("Tu numero $numberToSearch no está en la lista.")
        }

    }
}
